/*
 *  ======== package.xs ========
 *
 */

/*
 *  ======== getLibs ========
 */
function getLibs(prog)
{
    var lib = null;
  
    if (prog.build.target.isa == "64P") {        
        if ( this.JPEGENC.watermark == false ) {
                lib = "lib/dmjpge_tigem.l64P";
        }
        else {
                lib = null;
        }
        print("    will link with " + this.$name + ":" + lib);
    }
    return (lib);
}

/*
 *  ======== getSects ========
 */
function getSects()
{
    var template = null;

    if (Program.build.target.isa == "64P") {
        template = "ti/sdo/codecs/jpegenc/link.xdt";
    }

    return (template);
}
