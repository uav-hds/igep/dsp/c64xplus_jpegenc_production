/******************************************************************************
            TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
                                                                             
   Property of Texas Instruments  
   For  Unrestricted  Internal  Use  Only 
   Unauthorized reproduction and/or distribution is strictly prohibited.  
   This product  is  protected  under  copyright  law  and  trade  secret law 
   as an unpublished work.  

   Created 2004, (C) Copyright 2004 Texas Instruments.  All rights reserved.
                                                           
   Filename         : davinciDsp.cmd

   Description      : Common linker command file for the DSP software 
   
   Project          : DaVinci

  *******************************************************************************/


-stack    0x20000    /* Primary stack size   */
-heap     0x2400000 /*    Heap area size       */
-c                  /* Use C linking conventions: auto-init vars at runtime */

MEMORY
{
    DSPDATA        org = 0x80000000  len= 0x09600000
}

SECTIONS
{   
     
     .text 	: >  DSPDATA      
     .far 	: >  DSPDATA     
     .stack  	: >  DSPDATA
     .sysmem 	: >  DSPDATA
     .data   	: >  DSPDATA
     .bss    	: >  DSPDATA
     .cinit  	: >  DSPDATA
     .cio    	: >  DSPDATA
     .pinit  	: >  DSPDATA
     .const  	: >  DSPDATA
     .switch 	: >  DSPDATA 
     .bios   	: >  DSPDATA
     .alignconst: >  DSPDATA 
     .ccdData	: >  DSPDATA
     .bitsData	: >  DSPDATA
     .ref_buffer: >  DSPDATA
       
  
}                             

 
