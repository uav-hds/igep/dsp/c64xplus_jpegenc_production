xdc.loadCapsule('xdc/om2.xs');

var $om = xdc.om;
var __CFG__ = $om.$name == 'cfg';
var __ROV__ = $om.$name == 'rov';
var $$pkgspec = xdc.$$ses.findPkg('dm6446_jpegenc_2_00_002_production');

/* ======== IMPORTS ======== */

    xdc.loadPackage('ti.sdo.codecs.jpegenc');
    xdc.loadPackage('ti.sdo.codecs.jpegenc.ce');
    xdc.loadPackage('xdc');
    xdc.loadPackage('xdc.corevers');

/* ======== OBJECTS ======== */

// package dm6446_jpegenc_2_00_002_production
    var pkg = $om.$$bind('dm6446_jpegenc_2_00_002_production.Package', $$PObj());
    $om.$$bind('dm6446_jpegenc_2_00_002_production', $$VObj('dm6446_jpegenc_2_00_002_production', pkg));

/* ======== CONSTS ======== */


/* ======== CREATES ======== */


/* ======== FUNCTIONS ======== */


/* ======== SIZES ======== */


/* ======== TYPES ======== */


/* ======== ROV ======== */

if (__ROV__) {


} // __ROV__

/* ======== SINGLETONS ======== */

// package dm6446_jpegenc_2_00_002_production
    var po = $om['dm6446_jpegenc_2_00_002_production.Package'].$$init('dm6446_jpegenc_2_00_002_production.Package', $om['xdc.IPackage.Module']);
    po.$$bind('$capsule', undefined);
    var pkg = $om['dm6446_jpegenc_2_00_002_production'].$$init(po, 'dm6446_jpegenc_2_00_002_production', $$DEFAULT, false);
    $om.$packages.$add(pkg);
    pkg.$$bind('$name', 'dm6446_jpegenc_2_00_002_production');
    pkg.$$bind('$category', 'Package');
    pkg.$$bind('$$qn', 'dm6446_jpegenc_2_00_002_production.');
    pkg.$$bind('$spec', $$pkgspec);
    pkg.$$bind('$vers', [1, 0, 0]);
    pkg.$attr.$seal('length');
    pkg.$$bind('$imports', [
        ['ti.sdo.codecs.jpegenc', []],
        ['ti.sdo.codecs.jpegenc.ce', []],
    ]);
    if (pkg.$vers.length >= 3) {
        pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));
    }
    
    pkg.build.libraries = [
    ];
    
    pkg.build.libDesc = [
    ];

/* ======== INITIALIZATION ======== */

if (__CFG__) {
} // __CFG__
    pkg.init();
