#include <xdc/std.h>
#ifndef __config__
__FAR__ char dm6446_jpegenc_2_00_002_production__dummy__;
#define __xdc_PKGVERS 1, 0, 0
#define __xdc_PKGNAME dm6446_jpegenc_2_00_002_production
#define __xdc_PKGPREFIX dm6446_jpegenc_2_00_002_production_
#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

#else

#endif
